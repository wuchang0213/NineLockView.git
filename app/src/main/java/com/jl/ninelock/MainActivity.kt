package com.jl.ninelock

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,NineLockListener{

    private var resultDialog:ResultDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nine.setOnClickListener { nine.invalidate() }
        nine.setLockListener(this)

        resultDialog= ResultDialog(this)
    }

    override fun onLockResult(result: IntArray?) {
        val stringBuffer=StringBuffer()
        for(i in 0  until result!!.size){
            stringBuffer.append(result[i])
            System.out.print("${result[i]}  -> ")
        }
        resultDialog!!.setTitle(stringBuffer.toString())
        System.out.println()
        resultDialog!!.show()
    }

    override fun onError() {
        System.out.println("请重新绘制图案")
    }
}
